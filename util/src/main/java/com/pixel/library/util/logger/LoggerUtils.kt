package com.pixel.library.util.logger

import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.pixel.library.util.BuildConfig

@Suppress("SpreadOperator")
object LoggerUtils {

    fun init() {
        if (BuildConfig.DEBUG) {
            Logger.addLogAdapter(AndroidLogAdapter())
        }
    }

    fun d(s: String?, vararg objects: Any?) {
        Logger.d(s!!, *objects)
    }

    fun e(s: String?, vararg objects: Any?) {
        Logger.e(s!!, *objects)
    }
}
