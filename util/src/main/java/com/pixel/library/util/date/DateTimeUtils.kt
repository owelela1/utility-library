package com.pixel.library.util.date

import com.pixel.library.util.logger.LoggerUtils
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.text.DecimalFormat
import java.text.ParseException
import java.text.DateFormat
import java.text.NumberFormat

import java.time.Duration
import java.time.LocalTime
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import java.util.Calendar.DAY_OF_YEAR
import java.util.Calendar.YEAR
import java.util.Calendar.getInstance
import kotlin.math.abs

@SuppressWarnings("TooManyFunctions")
object DateTimeUtils {

    private const val MORNING = 8
    private const val MID_DAY = 12
    private const val EVENING = 17
    private const val TWENTY_FOUR = 24
    private const val SIXTY = 60
    private const val TEN = 10
    private const val HUNDRED = 100
    private const val THOUSAND = 1000
    private const val TEN_THOUSAND = 10000
    private const val THIRTY_SIX_THOUSAND = 3600
    private const val ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    fun isValidDateRange(startDate: Date, endDate: Date, strict: Boolean = false): Boolean {
        return if (strict) {
            startDate > endDate
        } else {
            startDate >= endDate
        }
    }

    fun isInValidTime(hr: Int, min: Int, date: Date): Boolean {
        var isInValidTime = false
        val dateFormat = SimpleDateFormat("hh:mm a", Locale.US)
        val localTime = dateFormat.format(date)

        LoggerUtils.e("check....$localTime.....$hr.....$min")

        if (localTime.contains("AM") && hr < MORNING) {
            isInValidTime = true
        } else if (localTime.contains("PM")) {
            if (hr == EVENING && min > 0) {
                isInValidTime = true
            } else if (hr > EVENING) {
                isInValidTime = true
            }
        }
        return isInValidTime
    }

    fun isTimeFuture(setted: Date, now: Date): Boolean {
        var isTimeFuture = true
        val dateFormat = SimpleDateFormat("hh:mm", Locale.US)

        dateFormat.timeZone = TimeZone.getTimeZone("GMT+03:00")

        val setDate = dateFormat.format(setted)
        val setTime = setDate.split(":")
        val setHour = setTime[0].trim()
        val setMin = setTime[1].trim()
        val currentDate = dateFormat.format(now)
        val currentTime = currentDate.split(":")
        val currentHour = currentTime[0].trim()
        val currentMin = currentTime[1].trim()

        val dateFormat2 = SimpleDateFormat("hh:mm a", Locale.US)

        val setEnd = dateFormat2.format(setted)
        val nowEnd = dateFormat2.format(now)

        if (setEnd.contains("AM") && nowEnd.contains("AM")) {
            isTimeFuture = !(setHour <= currentHour && setMin <= currentMin)
        } else if (setEnd.contains("PM") && nowEnd.contains("PM")) {
            isTimeFuture = if (setHour == MID_DAY.toString()) {
                false
            } else {
                !(setHour <= currentHour && setMin <= currentMin)
            }
        } else if (setEnd.contains("PM") && nowEnd.contains("AM")) {
            isTimeFuture = true
        } else if (setEnd.contains("AM") && nowEnd.contains("PM")) {
            isTimeFuture = false
        }
        return isTimeFuture
    }

    /**
    @Param startA
    @param endA
    @param startB
    @param endB
    @param strict boolean value specifying to check edge overlap.
    @return returns if the two date ranges overlap or not
     **/
    fun isOverlapping(
        startA: Date,
        endA: Date,
        startB: Date,
        endB: Date,
        strict: Boolean = false
    ): Boolean {
        return if (strict) {
            startA < endB && endA > startB
        } else {
            startA <= endB && endA >= startB
        }
    }

    /**
     * param milliseconds
     * return formatted string in hours, minute and seconds.
     */
    fun formatElapsedTime(milliseconds: Long): String {
        val totalSeconds = milliseconds / THOUSAND

        val hours = totalSeconds / THIRTY_SIX_THOUSAND
        val minutes = (totalSeconds % THIRTY_SIX_THOUSAND) / SIXTY
        val seconds = totalSeconds % SIXTY

        // format them.
        return when {
            hours > 0 && minutes > 0 -> {
                "$hours hours and $minutes minutes"
            }
            hours > 0 && minutes <= 0 -> {
                "$hours hours"
            }
            hours <= 0 && minutes > 0 -> {
                "$minutes minutes"
            }
            else -> "$seconds seconds"
        }
    }

    /**
     * param milliseconds
     * return formatted string in hours, minute and seconds.
     */
    fun formatFloatTime(rawTime: Float): String {

        val hour = rawTime.toString()
        val timeTo = hour.split(".")

        val hours = Integer.parseInt(timeTo[0].trim())

        val df = DecimalFormat("##")
        df.roundingMode = RoundingMode.HALF_EVEN

        val minRaw = (rawTime - hours) * SIXTY
        var minutes: Int
        try {
            minutes = df.format(minRaw).toInt()
        } catch (exception: NumberFormatException) {
            minutes = 0
            LoggerUtils.e(exception.message)
        }
        val minT = (rawTime - hours) * HUNDRED

        var minutesR: Int

        try {
            minutesR = df.format(minT).toInt() / HUNDRED
        } catch (exception: NumberFormatException) {
            minutesR = 0
            LoggerUtils.e(exception.message)
        }
        val secondsR = (rawTime - (hours + minutesR) * TEN_THOUSAND)

        val seconds = df.format(secondsR).toInt()

        // format them.
        return when {
            hours > 0 && minutes > 0 -> {
                "$hours hours and $minutes minutes"
            }
            hours > 0 && minutes <= 0 -> {
                "$hours hours"
            }
            hours <= 0 && minutes > 0 -> {
                "$minutes minutes"
            }
            else -> {
                "$seconds seconds"
            }
        }
    }

    fun formatToHHMMSS(date: Date): String {
        val formatter: DateFormat = SimpleDateFormat("hh:mm:ss a", Locale.US)
        return formatter.format(date)
    }

    fun formatToMMMDDHHMM(date: Date): String {
        val formatter: DateFormat = SimpleDateFormat("MMM dd hh:mm a", Locale.US)
        return formatter.format(date)
    }

    /**
     * params date
     * returns date formatted as Mar 20
     */
    fun formatToMMMDDD(date: Date): String {
        val formatter: DateFormat = SimpleDateFormat("MMM dd", Locale.US)
        return formatter.format(date)
    }

    fun getCurrentDateISOFormat(): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm'Z'", Locale.US)
        return df.format(Date())
    }

    fun parseDateIsoFormat(date: String): Date? {
        return try {
            SimpleDateFormat(ISO_DATE_TIME_FORMAT, Locale.US)
                .parse(date.replace("Z$".toRegex(), "+0000"))
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    @Suppress("TooGenericExceptionCaught")
    fun parseDateFromYYYYMMDD(date: String): Date? {

        val df = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        return try {
            df.parse(date)
        } catch (e: Exception) {
            LoggerUtils.e("Exception: $e")
            null
        }
    }

    fun getDifferenceHrFromTwoHrs(start: String?, end: String?): Int {

        val hr: Int
        val df12 = SimpleDateFormat("hh:mm a", Locale.US)
        val df24 = SimpleDateFormat("HH:mm", Locale.US)

        if (start == end) {
            hr = 0
        } else if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) {

            var date1 = df12.parse(start)
            var date2 = df12.parse(end)

            if (start!!.contains("AM") && end!!.contains("PM")) {

                date1 = df24.parse(start)
                date2 = df24.parse(end)
            } else if (end!!.contains("AM") && start.contains("PM")) {

                date1 = df24.parse(start)
                date2 = df24.parse(end)
            }

            val difference = date2.time - date1.time
            val days = (difference / (THOUSAND * SIXTY * SIXTY * TWENTY_FOUR)).toInt()
            var hours = ((difference - THOUSAND * SIXTY * SIXTY * TWENTY_FOUR
                    * days) / (THOUSAND * SIXTY * SIXTY)).toInt()
            hours = if (hours < 0) -hours else hours

            hr = hours
        } else {

            val from = LocalTime.parse(df24.format(df12.parse(start)))
            val to = LocalTime.parse(df24.format(df12.parse(end)))

            if (from.toString() == "00:00" && start == "12:00 AM") {
                val t = to.toString()
                val timeTo = t.split(":")

                val hrT = Integer.parseInt(timeTo[0].trim())

                hr = (TWENTY_FOUR - hrT)
            } else if (to.toString() == "00:00" && end == "12:00 AM") {

                val fr = from.toString()
                val timeFr = fr.split(":")

                val hrF = Integer.parseInt(timeFr[0].trim())

                hr = (TWENTY_FOUR - hrF)
            } else {
                val duration = Duration.between(from, to)
                hr = abs(duration.seconds / THIRTY_SIX_THOUSAND).toInt()
            }
        }

        return hr
    }

    fun floatElapsedTime(milliseconds: Long): Float {
        val totalSeconds = milliseconds / THOUSAND

        val hours = (totalSeconds / THIRTY_SIX_THOUSAND).toInt()
        val minutes = (totalSeconds % THIRTY_SIX_THOUSAND) / SIXTY
        val seconds = totalSeconds % SIXTY
        val min = if (minutes.toString().length >= 2) {
            Integer.parseInt(("" + minutes).substring(0, 2))
        } else {
            minutes.toInt()
        }

        val minute = ((min.toDouble() / SIXTY) * TEN).toInt()

        val final = (hours.toFloat() + (minute.toDouble() / TEN)).toFloat()

        val formatter: NumberFormat = NumberFormat.getInstance(Locale.US)
        formatter.maximumFractionDigits = 1
        formatter.minimumFractionDigits = 1
        formatter.roundingMode = RoundingMode.HALF_UP

        val formatFloat: Float = formatter.format(final).toFloat()

        // format them.
        return abs(formatFloat)
    }

    /**
     * params, a, b Date object
     * returns true if the two dates are equal ( to day level ), truncating hour, minute, seconds and millisecond.
     */
    fun sameDay(a: Date?, b: Date?): Boolean {
        val cal1 = getInstance()
        val cal2 = getInstance()
        cal1.time = a
        cal2.time = b
        return cal1[DAY_OF_YEAR] === cal2[DAY_OF_YEAR] &&
                cal1[YEAR] === cal2[YEAR]
    }
}
