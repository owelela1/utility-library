package com.pixel.library.util.network.monitoring.core

import android.net.ConnectivityManager
import android.net.LinkProperties
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi

/**
 * Implementation of ConnectivityManager.NetworkCallback,
 * it stores every change of connectivity into NetworkState
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
internal class NetworkCallbackImp(private val stateHolder: NetworkStateImp) :
    ConnectivityManager.NetworkCallback() {

    private fun updateConnectivity(isAvailable: Boolean, network: Network) {
        stateHolder.network = network
        stateHolder.isAvailable = isAvailable
    }

    // in case of a new network ( wifi enabled ) this is called first
    override fun onAvailable(network: Network) {
        Log.d(TAG, "$[$network] - new network")
        updateConnectivity(true, network)
    }

    // this is called several times in a row, as capabilities are added step by step
    override fun onCapabilitiesChanged(network: Network, networkCapabilities: NetworkCapabilities) {
        Log.d(TAG, "$[$network] - network capability changed: $networkCapabilities")
        stateHolder.networkCapabilities = networkCapabilities
    }

    // this is called after
    override fun onLost(network: Network) {
        Log.d(TAG, "$[$network] - network lost")
        updateConnectivity(false, network)
    }

    override fun onLinkPropertiesChanged(network: Network, linkProperties: LinkProperties) {
        stateHolder.linkProperties = linkProperties
        Log.d(TAG, "$[$network] - link changed: ${linkProperties.interfaceName}")
    }

    override fun onUnavailable() {
        Log.d(TAG, "Unavailable")
    }

    override fun onLosing(network: Network, maxMsToLive: Int) {
        Log.d(TAG, "$[$network] - Losing with $maxMsToLive")
    }

//    override fun onBlockedStatusChanged(network: Network, blocked: Boolean) {
//        AppLogger.d("$TAG: [$network] - Blocked status changed: $blocked")
//    }

    companion object {
        private const val TAG = "NetworkCallbackImp"
    }
}
