package com.pixel.library.util.network.monitoring

/**
 * Enables synchronous and asynchronous connectivity state checking thanks to LiveData and stored states.
 * @see isConnected to get the instance connectivity state
 * @see NetworkEvents to observe connectivity changes
 */
interface ConnectivityState {
    /**
     * Stored connectivity state of the device
     * True if the device has a available network
     */
    val isConnected: Boolean
        get() = networkStats.any {
            it.isAvailable
        }

    val isWiFi: Boolean
        get() = networkStats.any {
            it.isWifi
        }

    val isMobile: Boolean
        get() = networkStats.any {
            it.isMobile
        }

    /**
     * The stats of the networks being used by the device
     */
    val networkStats: Iterable<NetworkState>
}
