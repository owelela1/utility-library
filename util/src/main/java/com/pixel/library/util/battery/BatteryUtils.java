package com.pixel.library.util.battery;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class BatteryUtils {

    public static boolean isBatteryStatusGood(Activity activity, int minCharge) {

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = activity.registerReceiver(null, intentFilter);

        if (batteryStatus == null){
            return false;
        }

        int statusLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int statusCharge = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        if (statusLevel < minCharge) {
            return statusCharge == BatteryManager.BATTERY_STATUS_CHARGING;
        }

        return true;
    }
}
