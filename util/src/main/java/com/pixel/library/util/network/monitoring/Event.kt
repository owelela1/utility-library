package com.pixel.library.util.network.monitoring

import android.net.LinkProperties
import android.net.NetworkCapabilities

sealed class Event {

    class ConnectivityEvent(val isConnected: Boolean) : Event()

    val networkState: ConnectivityState = ConnectivityStateHolder

    object ConnectivityLost : Event()
    object ConnectivityAvailable : Event()
    data class NetworkCapabilityChanged(val old: NetworkCapabilities?) : Event()
    data class LinkPropertyChanged(val old: LinkProperties?) : Event()
}
