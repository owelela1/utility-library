package com.pixel.library.util.battery

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Bundle
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class BatteryUtilsTest {

    companion object {


        // region constants ------------------------------------------------------------------------

        private const val MIN_CHARGE = 20

        // endregion constants ---------------------------------------------------------------------


        // region helper fields --------------------------------------------------------------------

        @Mock
        var mMockActivity: Activity? = null

        @Mock
        var mMockBroadcastReceiver: BroadcastReceiver? = null

        @Mock
        var mMockBundle: Bundle? = null

        // endregion helper fields -----------------------------------------------------------------

        private var SUT: BatteryUtils? = null
    }

    @Before
    fun setup() {
        SUT = BatteryUtils()
    }

    @Test
    fun batteryCheck_success() {

        //Act
        val result = BatteryUtils.isBatteryStatusGood(mMockActivity, -2)
        MatcherAssert.assertThat(result, CoreMatchers.`is`(false))
    }

    @Test
    fun batteryCheck_failure_status() {

        //Arrange
        val activity = mMockActivity
        val filter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val batteryReceiver = mMockBroadcastReceiver
        val bundle = mMockBundle
        bundle!!.putInt(BatteryManager.EXTRA_LEVEL, MIN_CHARGE)
        bundle.putInt(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_CHARGING)
        batteryReceiver!!.setResultExtras(bundle)
        activity!!.registerReceiver(batteryReceiver, filter)

        //Act
        val result = BatteryUtils.isBatteryStatusGood(mMockActivity, MIN_CHARGE)
        MatcherAssert.assertThat(result, CoreMatchers.`is`(false))
    }
}